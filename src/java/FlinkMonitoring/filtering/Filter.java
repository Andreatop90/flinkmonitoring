package FlinkMonitoring.filtering;

import FlinkMonitoring.Entity.SemaphoreSTD;
import FlinkMonitoring.ReflectionUtil;
import com.google.gson.Gson;
import org.apache.flink.api.common.functions.FilterFunction;

public class Filter {

    public static class FilterTuple implements FilterFunction<String>{

        public boolean filter(String s) throws Exception {
            Gson gson = new Gson();

            SemaphoreSTD semaphoreSTD=gson.fromJson(s,SemaphoreSTD.class);

            return ReflectionUtil.checkFieldNull(semaphoreSTD);
        }
    }
}
