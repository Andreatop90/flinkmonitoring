package FlinkMonitoring;

import FlinkMonitoring.Entity.*;
import FlinkMonitoring.filtering.Filter;
import com.google.gson.Gson;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;

import java.util.Properties;

public class Main {

    public static void main(String args[]) throws Exception {

        //Upload Configuration File
        S3Manager.setGlobalsVariable("config-system","config.json");
        String brokers=new String();
        int i=0;
        for (String s: Global.BROKERS) {
            brokers=brokers.concat(s);
            brokers=brokers.concat(",");
            i++;
        }
        try{
            if (i==1)
                brokers=brokers.substring(0,brokers.length()-1);
            if (i>1)
            brokers=brokers.substring(0,brokers.length()-2);

        }
        catch (Exception e){
            System.err.println(e.getCause());
            System.out.println("non ci sono broker");
            return;
        }
        System.out.println(brokers);

        final StreamExecutionEnvironment env=StreamExecutionEnvironment.getExecutionEnvironment();

        //consumer kafka
        Properties properties=new Properties();
        //properties.setProperty("bootstrap.servers","34.243.170.15:9092");
        properties.setProperty("bootstrap.servers",brokers);

        properties.setProperty("zookeeper.connect",Global.ZOOKEEPER);
        //properties.setProperty("groupid","monitoring");
        properties.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");

        FlinkKafkaConsumer010<String> semaphoreStreaming=new FlinkKafkaConsumer010<String>(Global.TOPIC_CONSUMER_SEMAPHORE,new SimpleStringSchema(),properties);

        FlinkKafkaConsumer010<String> mobileStreaming=new FlinkKafkaConsumer010<String>(Global.TOPIC_CONSUMER_MOBILE,new SimpleStringSchema(),properties);


        //producer kafka

        FlinkKafkaProducer010<String> producer15Minutes=new FlinkKafkaProducer010<String>(
                brokers, Global.TOPIC_PRODUCER_QUERY1,new SimpleStringSchema());
        producer15Minutes.setLogFailuresOnly(false);
        producer15Minutes.setFlushOnCheckpoint(true);


        FlinkKafkaProducer010<String> producerSecondQuery=new FlinkKafkaProducer010<String>(
                brokers, Global.TOPIC_PRODUCER_QUERY2,new SimpleStringSchema());
        producerSecondQuery.setLogFailuresOnly(false);
        producerSecondQuery.setFlushOnCheckpoint(true);

        FlinkKafkaProducer010<String> producerThirdQuery=new FlinkKafkaProducer010<String>(
                brokers, Global.TOPIC_PRODUCER_QUERY3,new SimpleStringSchema());
        producerThirdQuery.setLogFailuresOnly(false);
        producerThirdQuery.setFlushOnCheckpoint(true);

        FlinkKafkaProducer010<String> producerMape=new FlinkKafkaProducer010<String>(
                brokers, Global.TOPIC_PRODUCER_MAPE,new SimpleStringSchema());
        producerMape.setLogFailuresOnly(false);
        producerMape.setFlushOnCheckpoint(true);



        DataStream<CrossroadMapped> streamCrossroadMapped=env.addSource(semaphoreStreaming).name("sourceCrossroad")
                .filter(new Filter.FilterTuple())
                .map(new MapFunction<String, CrossroadMapped>() {
                    public CrossroadMapped map(String s) throws Exception {
                        CrossroadMapped crossroadMapped=new CrossroadMapped();
                        Gson gson = new Gson();
                        SemaphoreSTD semaphoreSTD=gson.fromJson(s,SemaphoreSTD.class);
                        crossroadMapped.setIdCrossroad(semaphoreSTD.getIdCrossroad());
                        crossroadMapped.setAvgSpeedCrossroad(semaphoreSTD.getAvgSpeed());
                        crossroadMapped.setIdSemaphore(semaphoreSTD.getId());
                        crossroadMapped.setVehiclePassed(semaphoreSTD.getVeiclesPassed());
                        return crossroadMapped;
                    }
                });

        DataStream<MobileDeviceMessage> mobileDeviceMessageDataStream=env.addSource(mobileStreaming).uid("sourceMobileDevice")
                .filter(new FilterFunction<String>() {
                    @Override
                    public boolean filter(String s) throws Exception {
                        Gson gson = new Gson();
                        MobileDeviceMessage mobileDeviceMessage=gson.fromJson(s,MobileDeviceMessage.class);
                        return ReflectionUtil.checkFieldNull(mobileDeviceMessage);
                    }
                })
                .map(new MapFunction<String, MobileDeviceMessage>() {
                    @Override
                    public MobileDeviceMessage map(String s) throws Exception {
                        MobileDeviceMessage mobileDeviceMessage=new MobileDeviceMessage();
                        Gson gson=new Gson();
                        mobileDeviceMessage=gson.fromJson(s,MobileDeviceMessage.class);
                        return mobileDeviceMessage;
                    }
                });


        PrimaQuery.executePrimaQuery(streamCrossroadMapped,producer15Minutes);


        SecondaQuery.executeSecondQuery(streamCrossroadMapped,producerSecondQuery);

        TerzaQuery.executeThirdQuery(mobileDeviceMessageDataStream,producerThirdQuery);

        CicloMape.executeMape(streamCrossroadMapped,producerMape,30);



        env.execute();

    }
}
