package FlinkMonitoring;

import FlinkMonitoring.Entity.CrossroadMapped;
import FlinkMonitoring.Entity.Rank;
import com.google.gson.Gson;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Date;
import java.util.TreeSet;

public class PrimaQuery {

    public static void executePrimaQuery(DataStream<CrossroadMapped> dataStream,FlinkKafkaProducer010<String> producer){
        SingleOutputStreamOperator<Tuple3<String, Double, Double>> firstWindow=dataStream
                .map(new MapFunction<CrossroadMapped, Tuple3<String,Double,Double>>() {
                    @Override
                    public Tuple3<String, Double, Double> map(CrossroadMapped crossroadMapped) throws Exception {
                        return new Tuple3<>(crossroadMapped.getIdCrossroad(),crossroadMapped.getAvgSpeedCrossroad(),1D);
                    }
                })
                .keyBy(0)
                .timeWindow(Time.minutes(15))
                .reduce(new ReduceFunction<Tuple3<String, Double, Double>>() {
                    @Override
                    public Tuple3<String, Double, Double> reduce(Tuple3<String, Double, Double> stringDoubleDoubleTuple3, Tuple3<String, Double, Double> t1) throws Exception {
                        return new Tuple3<>(stringDoubleDoubleTuple3.f0,stringDoubleDoubleTuple3.f1+t1.f1,stringDoubleDoubleTuple3.f2+t1.f2);
                    }
                });

        printWindow(firstWindow,1,producer,"query1finestra1");

        SingleOutputStreamOperator<Tuple3<String, Double, Double>> secondWindow=firstWindow
                .keyBy(0)
                .timeWindow(Time.hours(1))
                .reduce(new ReduceFunction<Tuple3<String, Double, Double>>() {
                    @Override
                    public Tuple3<String, Double, Double> reduce(Tuple3<String, Double, Double> stringDoubleDoubleTuple3, Tuple3<String, Double, Double> t1) throws Exception {
                        return new Tuple3<>(stringDoubleDoubleTuple3.f0,stringDoubleDoubleTuple3.f1+t1.f1,stringDoubleDoubleTuple3.f2+t1.f2);
                    }
                });

        printWindow(secondWindow,2,producer,"query1finestra2");

        SingleOutputStreamOperator<Tuple3<String, Double, Double>> thirdWindow=secondWindow
                .keyBy(0)
                .timeWindow(Time.days(1))
                .reduce(new ReduceFunction<Tuple3<String, Double, Double>>() {
                    @Override
                    public Tuple3<String, Double, Double> reduce(Tuple3<String, Double, Double> stringDoubleDoubleTuple3, Tuple3<String, Double, Double> t1) throws Exception {
                        return new Tuple3<>(stringDoubleDoubleTuple3.f0,stringDoubleDoubleTuple3.f1+t1.f1,stringDoubleDoubleTuple3.f2+t1.f2);
                    }
                });

        printWindow(thirdWindow,3,producer,"query1finestra3");




    }

    public static void printWindow(SingleOutputStreamOperator<Tuple3<String, Double, Double>> stream,int type,FlinkKafkaProducer010<String> producer,String name){
        stream
                .timeWindowAll(Time.seconds(2))
                .apply(new AllWindowFunction<Tuple3<String,Double,Double>, ArrayList<CrossroadMapped>, TimeWindow>() {
                    @Override
                    public void apply(TimeWindow timeWindow, Iterable<Tuple3<String, Double, Double>> iterable, Collector<ArrayList<CrossroadMapped>> collector) throws Exception {
                        CrossroadMapped crossroadMapped=new CrossroadMapped();
                        TreeSet<CrossroadMapped> treeSet=new TreeSet<CrossroadMapped>();
                        for (Tuple3<String,Double,Double> tuple3:iterable){
                            crossroadMapped=new CrossroadMapped();
                            crossroadMapped.setIdCrossroad(tuple3.f0);
                            crossroadMapped.setAvgSpeedCrossroad(tuple3.f1/tuple3.f2);
                            treeSet.add(crossroadMapped);
                        }
                        ArrayList<CrossroadMapped> arrayList=new ArrayList<>();
                        int i=0;
                        for (CrossroadMapped crossroadMapped1:treeSet){
                            if (i==10)
                                break;
                            i++;
                            arrayList.add(crossroadMapped1);
                        }
                        collector.collect(arrayList);
                    }
                })
                .map(new MapFunction<ArrayList<CrossroadMapped>, Tuple2<ArrayList<CrossroadMapped>,Integer>>() {
                    @Override
                    public Tuple2<ArrayList<CrossroadMapped>, Integer> map(ArrayList<CrossroadMapped> crossroadMappeds) throws Exception {
                        return new Tuple2<>(crossroadMappeds,0);
                    }
                })
                .keyBy(1)
                .flatMap(new RichFlatMapFunction<Tuple2<ArrayList<CrossroadMapped>,Integer>, ArrayList<CrossroadMapped>>() {
                    private transient ValueState<Tuple2<ArrayList<CrossroadMapped>, Integer>> state;

                    @Override
                    public void flatMap(Tuple2<ArrayList<CrossroadMapped>, Integer> arrayListIntegerTuple2, Collector<ArrayList<CrossroadMapped>> collector) throws Exception {
                        Tuple2<ArrayList<CrossroadMapped>,Integer> myState=state.value();
                        if (!myState.f0.equals(arrayListIntegerTuple2.f0)){
                            state.update(arrayListIntegerTuple2);
                            collector.collect(arrayListIntegerTuple2.f0);
                        }
                    }

                    @Override
                    public void open(Configuration config) {
                        ValueStateDescriptor<Tuple2<ArrayList<CrossroadMapped>,Integer>> descriptor =
                                new ValueStateDescriptor<>(
                                        "state", // the state name
                                        TypeInformation.of(new TypeHint<Tuple2<ArrayList<CrossroadMapped>,Integer>>() {
                                        }), // type information
                                        Tuple2.of(new ArrayList<>(),0)); // default value of the state, if nothing was set
                        state = getRuntimeContext().getState(descriptor);
                    }
                })
                .uid(name)

                .map(new MapFunction<ArrayList<CrossroadMapped>, String>() {
                    @Override
                    public String map(ArrayList<CrossroadMapped> crossroadMappeds) throws Exception {
                        Rank rank=new Rank();


                        rank.setTimestamp(new Date());
                        rank.setType(type);
                        rank.setCrossroads(crossroadMappeds);

                        Gson gson=new Gson();
                        //System.out.println(gson.toJson(rank));
                        return gson.toJson(rank);
                    }
                }).addSink(producer);
    }
}
