package FlinkMonitoring;

import FlinkMonitoring.Entity.CrossroadMapped;
import FlinkMonitoring.Entity.MapeClass;
import com.google.gson.Gson;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class CicloMape {

    public static void executeMape(DataStream<CrossroadMapped> dataStream,FlinkKafkaProducer010<String> producer,Integer minutes){

            dataStream
                    .filter(new FilterFunction<CrossroadMapped>() {
                        @Override
                        public boolean filter(CrossroadMapped crossroadMapped) throws Exception {
                            return crossroadMapped.getVehiclePassed()!=null;
                        }
                    })
                    .keyBy("idCrossroad","idSemaphore")
                    .window(TumblingProcessingTimeWindows.of(Time.minutes(minutes)))
                    .aggregate(new AggregateFunction<CrossroadMapped, Tuple3<CrossroadMapped,Double,Integer>, CrossroadMapped>() {
                        @Override
                        public Tuple3<CrossroadMapped, Double, Integer> createAccumulator() {
                            return new Tuple3<CrossroadMapped,Double,Integer>(new CrossroadMapped(),(double)0,0);
                        }

                        @Override
                        public Tuple3<CrossroadMapped, Double, Integer> add(CrossroadMapped crossroadMapped, Tuple3<CrossroadMapped, Double, Integer> crossroadMappedDoubleIntegerTuple3) {
                            crossroadMappedDoubleIntegerTuple3.f0=crossroadMapped;
                            crossroadMappedDoubleIntegerTuple3.f1+=(double)crossroadMapped.getVehiclePassed();
                            crossroadMappedDoubleIntegerTuple3.f2+=1;
                            return crossroadMappedDoubleIntegerTuple3;
                        }

                        @Override
                        public CrossroadMapped getResult(Tuple3<CrossroadMapped, Double, Integer> crossroadMappedDoubleIntegerTuple3) {

                            return crossroadMappedDoubleIntegerTuple3.f0;
                        }

                        @Override
                        public Tuple3<CrossroadMapped, Double, Integer> merge(Tuple3<CrossroadMapped, Double, Integer> crossroadMappedDoubleIntegerTuple3, Tuple3<CrossroadMapped, Double, Integer> acc1) {
                            crossroadMappedDoubleIntegerTuple3.f1+=acc1.f1;
                            crossroadMappedDoubleIntegerTuple3.f2+=acc1.f2;
                            return crossroadMappedDoubleIntegerTuple3;
                        }
                    })
                    .keyBy("idCrossroad")
                    .window(TumblingProcessingTimeWindows.of(Time.seconds(5)))
                    .apply(new WindowFunction<CrossroadMapped, MapeClass, Tuple, TimeWindow>() {
                        @Override
                        public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<CrossroadMapped> iterable, Collector<MapeClass> collector) throws Exception {
                            TreeSet<CrossroadMapped> treeSetOutput=new TreeSet<>(new Comparator<CrossroadMapped>() {
                                @Override
                                public int compare(CrossroadMapped o1, CrossroadMapped o2) {
                                    if (o1.getVehiclePassed()<o2.getVehiclePassed())
                                        return 1;
                                    else if (o1.getVehiclePassed()>o2.getVehiclePassed())
                                        return -1;
                                    else return 0;

                                }
                            });


                            iterable.forEach(crossroadMapped -> treeSetOutput.add(crossroadMapped));

                            ArrayList<CrossroadMapped> arrayList=new ArrayList<>(treeSetOutput);

                            collector.collect(new MapeClass(arrayList,arrayList.get(0).getIdCrossroad()));
                        }
                    })
                    .windowAll(TumblingProcessingTimeWindows.of(Time.seconds(5)))
                    .apply(new AllWindowFunction<MapeClass, TreeSet<MapeClass>, TimeWindow>() {
                        @Override
                        public void apply(TimeWindow timeWindow, Iterable<MapeClass> iterable, Collector<TreeSet<MapeClass>> collector) throws Exception {
                            TreeSet<MapeClass> treeSet=new TreeSet<>(new Comparator<MapeClass>() {
                                @Override
                                public int compare(MapeClass o1, MapeClass o2) {
                                    if (o1.getList().get(0).getVehiclePassed()<o2.getList().get(0).getVehiclePassed())
                                        return 1;
                                    else if (o1.getList().get(0).getVehiclePassed()>o2.getList().get(0).getVehiclePassed())
                                        return -1;
                                    else return 0;
                                }
                            });

                            iterable.forEach(mapeClass -> treeSet.add(mapeClass));

                            collector.collect(treeSet);

                        }
                    })
                    .map(new MapFunction<TreeSet<MapeClass>, String>() {
                        @Override
                        public String map(TreeSet<MapeClass> mapeClasses) throws Exception {



                            Gson gson=new Gson();
                            //System.out.println(gson.toJson(rank));
                            return gson.toJson(new ArrayList<MapeClass>(mapeClasses));

                        }
                    })
                    .addSink(producer);

            
            
    }
}
