package FlinkMonitoring;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionUtil {

    //return false if one of the fields is null
    public static boolean checkFieldNull(Object object) throws InvocationTargetException, IllegalAccessException {

        Method[] methods=object.getClass().getMethods();
        for (Method method:methods){
            if (isGetter(method)){
                if (checkNull(method.invoke(object)))
                    return false;
            }
        }
        return true;
    }

    private static boolean isGetter(Method method){
        if (!method.getName().startsWith("get"))
            return false;
        if (method.getParameterTypes().length!= 0)
            return false;
        if (void.class.equals(method.getReturnType()))
            return false;
        if (method.getName().equals("getClass"))
            return false;
        return true;
    }

    private static boolean isSetter(Method method){
        if (!method.getName().startsWith("set"))
            return false;
        if (method.getParameterTypes().length!=1)
            return false;
        return true;
    }

    private static <T> boolean checkNull(T field){
        if (field==null)
            return true;
        return false;
    }
}
