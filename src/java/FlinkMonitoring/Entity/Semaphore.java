package FlinkMonitoring.Entity;

import java.sql.Timestamp;

public class Semaphore {
    protected double id;
    protected String idCrossroad;
    protected Position position;
    protected Timestamp timestamp;

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getIdCrossroad() {
        return idCrossroad;
    }

    public void setIdCrossroad(String idCrossroad) {
        this.idCrossroad = idCrossroad;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
