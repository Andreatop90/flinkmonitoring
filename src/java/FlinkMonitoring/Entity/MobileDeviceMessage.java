package FlinkMonitoring.Entity;

import com.mongodb.client.model.geojson.Point;

import java.sql.Timestamp;

public class MobileDeviceMessage {

    private String id;
    private Timestamp timestamp;
    private String idProximitySemaphore;
    private double instantSpeed;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getIdProximitySemaphore() {
        return idProximitySemaphore;
    }

    public void setIdProximitySemaphore(String idProximitySemaphore) {
        this.idProximitySemaphore = idProximitySemaphore;
    }

    public double getInstantSpeed() {
        return instantSpeed;
    }

    public void setInstantSpeed(double instantSpeed) {
        this.instantSpeed = instantSpeed;
    }
}