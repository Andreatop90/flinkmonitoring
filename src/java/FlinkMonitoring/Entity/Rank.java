package FlinkMonitoring.Entity;

import java.util.ArrayList;
import java.util.Date;

public class Rank {
    private String id;
    private Date timestamp;
    private Integer type;
    private ArrayList<CrossroadMapped> crossroads;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public ArrayList<CrossroadMapped> getCrossroads() {
        return crossroads;
    }

    public void setCrossroads(ArrayList<CrossroadMapped> crossroads) {
        this.crossroads = crossroads;
    }
}
