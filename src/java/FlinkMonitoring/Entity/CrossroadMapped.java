package FlinkMonitoring.Entity;

public class CrossroadMapped implements Comparable<CrossroadMapped> {

    private String idCrossroad;
    private Double idSemaphore;
    private Double avgSpeedCrossroad;
    private Integer vehiclePassed;
    private String name;

    public String getIdCrossroad() {
        return idCrossroad;
    }

    public void setIdCrossroad(String idCrossroad) {
        this.idCrossroad = idCrossroad;
    }

    public Double getAvgSpeedCrossroad() {
        return avgSpeedCrossroad;
    }

    public void setAvgSpeedCrossroad(Double avgSpeedCrossroad) {
        this.avgSpeedCrossroad = avgSpeedCrossroad;
    }

    public Integer getVehiclePassed() {
        return vehiclePassed;
    }

    public void setVehiclePassed(Integer vehiclePassed) {
        this.vehiclePassed = vehiclePassed;
    }

    public Double getIdSemaphore() {
        return idSemaphore;
    }

    public void setIdSemaphore(Double idSemaphore) {
        this.idSemaphore = idSemaphore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CrossroadMapped() {
    }

    public int compareTo(CrossroadMapped o) {
        if (this.avgSpeedCrossroad<o.getAvgSpeedCrossroad())
            return 1;
        if (this.avgSpeedCrossroad>o.getAvgSpeedCrossroad())
            return -1;
        else
            return 0;
    }
}
