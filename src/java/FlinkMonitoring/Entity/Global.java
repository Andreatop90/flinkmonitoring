package FlinkMonitoring.Entity;


import java.util.ArrayList;

public class Global {
    public static  String  TOPIC_CONSUMER_SEMAPHORE = "inputSemaphore";
    public static String TOPIC_CONSUMER_MOBILE= "inputMobile";
    public static String TOPIC_PRODUCER_QUERY1="rankQuery1";
    public static String TOPIC_PRODUCER_QUERY2="rankQuery2";
    public static String TOPIC_PRODUCER_QUERY3="rankQuery3";
    public static String TOPIC_PRODUCER_MAPE="topicMape";
    public static  String  ZOOKEEPER ="52.18.180.9:2181";
    public static ArrayList<String> BROKERS = new ArrayList<String>();

}
