package FlinkMonitoring.Entity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

public class ListaSecondaQuery {

    private ArrayList<CrossroadMapped> listOfCrossroad;
    private Double globalMediana;
    private Date timestamp;
    private Integer type;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public ArrayList<CrossroadMapped> getListOfCrossroad() {
        return listOfCrossroad;
    }

    public void setListOfCrossroad(ArrayList<CrossroadMapped> listOfCrossroad) {
        this.listOfCrossroad = listOfCrossroad;
    }

    public Double getGloablMediana() {
        return globalMediana;
    }

    public void setGloablMediana(Double globalMediana) {
        this.globalMediana = globalMediana;
    }

}
