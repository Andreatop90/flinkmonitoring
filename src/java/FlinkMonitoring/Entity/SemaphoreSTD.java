package FlinkMonitoring.Entity;

public class SemaphoreSTD extends Semaphore {
    private int greenLightTime;

    private int veiclesPassed;

    private double avgSpeed;

    public int getGreenLightTime() {
        return greenLightTime;
    }

    public void setGreenLightTime(int greenLightTime) {
        this.greenLightTime = greenLightTime;
    }

    public int getVeiclesPassed() {
        return veiclesPassed;
    }

    public void setVeiclesPassed(int veiclesPassed) {
        this.veiclesPassed = veiclesPassed;
    }

    public double getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(double avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    @Override
    public String toString() {
        String result=new String();
        result="MESSAGE SEMAPHORE STD:\n";
        result=result.concat("ID: "+this.getId()+"\n");
        result=result.concat("Crossroad ID: "+this.getIdCrossroad()+"\n");
        result=result.concat("Position: "+this.getPosition().getX()+", "+this.getPosition().getY()+"\n");
        result=result.concat("Timestamp: "+this.getTimestamp().toString()+"\n");
        result=result.concat("Velocita media: "+this.getAvgSpeed()+"\n");
        result=result.concat("Veicoli passati: "+this.getVeiclesPassed()+"\n");
        result=result.concat("Durata Verde: "+this.getGreenLightTime()+"\n");
        return result;
    }
}
