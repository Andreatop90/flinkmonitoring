package FlinkMonitoring.Entity;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class S3Manager {

    private static BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAJWEPBQEOTF6CTPXQ", "6GoGqLMYFWVco9x/HmmqbuAQnocCKlTv+Dhs3Atu");
    private static AmazonS3 s3Client;

    private static void downloadBucket(String bucketName,String bucketKey){
        s3Client.getObject(
                new GetObjectRequest(bucketName, bucketKey),
                new File("config.json")
        );
    }

    public static void setGlobalsVariable(String bktname, String key) throws IOException, ParseException {

        s3Client=AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds)).withRegion("eu-west-1")
                .build();
        downloadBucket(bktname,key);


        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader("config.json"));

        //ProducerSemaphore
        JSONObject ProducerSemaphore = (JSONObject) jsonObject.get("flinkMonitoring");
        //TopicSemaphores
        Global.TOPIC_CONSUMER_SEMAPHORE = (String) ProducerSemaphore.get("topicConsumer");
        Global.TOPIC_CONSUMER_MOBILE = (String) ProducerSemaphore.get("topicMobile");

        //Zookeeper IP & PORT
        Global.ZOOKEEPER = (String) ProducerSemaphore.get("zookeeper");
        Global.TOPIC_PRODUCER_QUERY1=(String)ProducerSemaphore.get("topicQuery1");
        Global.TOPIC_PRODUCER_QUERY2=(String)ProducerSemaphore.get("topicQuery2");
        Global.TOPIC_PRODUCER_QUERY3=(String)ProducerSemaphore.get("topicQuery3");
        Global.TOPIC_PRODUCER_MAPE=(String)ProducerSemaphore.get("topicMape");


        //Brokers IP $ PORT
        JSONArray brokersJson = (JSONArray) ProducerSemaphore.get("brokers");
        for (int i = 0; i <brokersJson.size() ; i++) {
            Global.BROKERS.add((String)brokersJson.get(i));
        }

    }
}
