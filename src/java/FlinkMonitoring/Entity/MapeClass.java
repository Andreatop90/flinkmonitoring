package FlinkMonitoring.Entity;

import java.util.ArrayList;
import java.util.Date;

public class MapeClass {

    private String idCrossroad;
    private ArrayList<CrossroadMapped> list;
    private Date timestamp;

    public String getIdCrossroad() {
        return idCrossroad;
    }

    public void setIdCrossroad(String idCrossroad) {
        this.idCrossroad = idCrossroad;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public ArrayList<CrossroadMapped> getList() {
        return list;
    }

    public void setList(ArrayList<CrossroadMapped> list) {
        this.list = list;
    }

    public MapeClass(ArrayList<CrossroadMapped> list,String idCrossroad) {
        this.idCrossroad=idCrossroad;
        this.list = list;
    }
}
