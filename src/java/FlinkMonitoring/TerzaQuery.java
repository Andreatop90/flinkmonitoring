package FlinkMonitoring;

import FlinkMonitoring.Entity.CrossroadMapped;
import FlinkMonitoring.Entity.MobileDeviceMessage;
import FlinkMonitoring.Entity.Rank;
import com.google.gson.Gson;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class TerzaQuery {

    public static void executeThirdQuery(DataStream<MobileDeviceMessage> dataStream, FlinkKafkaProducer010<String> producer){
        dataStream
                .filter(new FilterFunction<MobileDeviceMessage>() {
                    @Override
                    public boolean filter(MobileDeviceMessage mobileDeviceMessage) throws Exception {
                        return !mobileDeviceMessage.getIdProximitySemaphore().equals(String.valueOf(-1));
                    }
                })
                .keyBy("idProximitySemaphore")
                .timeWindow(Time.minutes(5))
                .apply(new WindowFunction<MobileDeviceMessage, Tuple4<String,Double,Integer,Double>, Tuple, TimeWindow>() {
                    //tuple4<id_semaphore,media,sum_speed,number,media_pesata>
                    @Override
                    public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<MobileDeviceMessage> iterable, Collector<Tuple4<String,Double,Integer,Double>> collector) throws Exception {
                        AtomicReference<Double> sumSpeed= new AtomicReference<>((double) 0);
                        AtomicInteger numberOfVehicle= new AtomicInteger();
                        AtomicReference<String> idSemaphore=new AtomicReference<>((String)"");

                        iterable.forEach(mobileDeviceMessage -> {
                            idSemaphore.set(mobileDeviceMessage.getIdProximitySemaphore());
                            numberOfVehicle.set(numberOfVehicle.get() + 1);
                            sumSpeed.set(sumSpeed.get() + mobileDeviceMessage.getInstantSpeed());
                        });

                        //sumSpeed.set(sumSpeed.get()/numberOfVehicle.get());
                        collector.collect(new Tuple4(idSemaphore.get(),sumSpeed.get(),numberOfVehicle.get(),sumSpeed.get()/numberOfVehicle.get()));
                    }
                })
                .uid("query3finestra1")
                .timeWindowAll(Time.seconds(2))
                .apply(new AllWindowFunction<Tuple4<String,Double,Integer,Double>, TreeSet<Tuple4<String,Double,Integer,Double>>, TimeWindow>() {
                    @Override
                    public void apply(TimeWindow timeWindow, Iterable<Tuple4<String, Double, Integer, Double>> iterable, Collector<TreeSet<Tuple4<String, Double, Integer, Double>>> collector) throws Exception {
                        TreeSet<Tuple4<String,Double,Integer,Double>> treeSet=new TreeSet<>(new Comparator<Tuple4<String, Double, Integer, Double>>() {
                            @Override
                            public int compare(Tuple4<String, Double, Integer, Double> o1, Tuple4<String, Double, Integer, Double> o2) {
                                if (o1.f3>o2.f3)
                                    return 1;
                                else if (o1.f3<o2.f3)
                                    return -1;
                                else return 0;
                            }
                        });

                        iterable.forEach(stringDoubleIntegerDoubleTuple4 -> treeSet.add(stringDoubleIntegerDoubleTuple4));
                        collector.collect(treeSet);
                    }
                })
                .map(new MapFunction<TreeSet<Tuple4<String,Double,Integer,Double>>, String>() {
                    @Override
                    public String map(TreeSet<Tuple4<String, Double, Integer, Double>> tuple4s) throws Exception {
                        Rank rank=new Rank();
                        ArrayList<CrossroadMapped> arrayList=new ArrayList<>();
                        tuple4s.forEach(stringDoubleIntegerDoubleTuple4 -> {
                            CrossroadMapped crossroadMapped=new CrossroadMapped();
                            crossroadMapped.setName(stringDoubleIntegerDoubleTuple4.f0);
                            crossroadMapped.setAvgSpeedCrossroad(stringDoubleIntegerDoubleTuple4.f1);
                            crossroadMapped.setVehiclePassed(stringDoubleIntegerDoubleTuple4.f2);

                            arrayList.add(crossroadMapped);
                        });
                        rank.setCrossroads(arrayList);
                        rank.setTimestamp(new Date());
                        Gson gson=new Gson();
                        return gson.toJson(rank);
                    }
                })
                .addSink(producer);


    }

}
