package FlinkMonitoring;

import FlinkMonitoring.Entity.CrossroadMapped;
import FlinkMonitoring.Entity.ListaSecondaQuery;
import com.google.gson.Gson;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;
import org.apache.flink.util.Collector;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class SecondaQuery {

    public static void executeSecondQuery(DataStream<CrossroadMapped> dataStream,FlinkKafkaProducer010<String> producer){

        SingleOutputStreamOperator<CrossroadMapped> firstWindow=dataStream
                .keyBy("idCrossroad")
                .window(TumblingProcessingTimeWindows.of(Time.minutes(15)))
                .reduce((crossroadMapped, t1) -> {
                    crossroadMapped.setVehiclePassed(crossroadMapped.getVehiclePassed()+t1.getVehiclePassed());
                    return crossroadMapped;
                });
        printWindow(firstWindow,1,producer,"query2finestra1");

        SingleOutputStreamOperator<CrossroadMapped> secondWindow=firstWindow
                .keyBy("idCrossroad")
                .window(TumblingProcessingTimeWindows.of(Time.hours(1)))
                .reduce((crossroadMapped, t1) -> {
                    crossroadMapped.setVehiclePassed(crossroadMapped.getVehiclePassed()+t1.getVehiclePassed());
                    return crossroadMapped;
                });
        printWindow(secondWindow,2,producer,"query2finestra2");

        SingleOutputStreamOperator<CrossroadMapped> thirdWindow=secondWindow
                .keyBy("idCrossroad")
                .window(TumblingProcessingTimeWindows.of(Time.days(1)))
                .reduce((crossroadMapped, t1) -> {
                    crossroadMapped.setVehiclePassed(crossroadMapped.getVehiclePassed()+t1.getVehiclePassed());
                    return crossroadMapped;
                });

        printWindow(thirdWindow,3,producer,"query2finestra3");
    }

    public static void printWindow(SingleOutputStreamOperator<CrossroadMapped> stream,int type,FlinkKafkaProducer010<String> producer,String name){
        stream
                .uid(name)
                .windowAll(TumblingProcessingTimeWindows.of(Time.seconds(2)))
                .apply(new AllWindowFunction<CrossroadMapped, Tuple2<ArrayList<CrossroadMapped>, Double>, TimeWindow>() {
                    @Override
                    public void apply(TimeWindow timeWindow, Iterable<CrossroadMapped> iterable, Collector<Tuple2<ArrayList<CrossroadMapped>, Double>> collector) throws Exception {

                        ArrayList<CrossroadMapped> myCrossroadUpper=new ArrayList<>();

                        TreeSet<CrossroadMapped> treeSet=new TreeSet<>(new Comparator<CrossroadMapped>() {
                            @Override
                            public int compare(CrossroadMapped o1, CrossroadMapped o2) {
                                if (o1.getVehiclePassed()>o2.getVehiclePassed())
                                    return 1;
                                else if(o1.getVehiclePassed()<o2.getVehiclePassed())
                                    return -1;
                                else
                                    return 0;
                            }
                        });


                        Double mediana=new Double(0);
                        iterable.forEach(crossroadMapped -> treeSet.add(crossroadMapped));

                        ArrayList<CrossroadMapped> arrayList=new ArrayList<>(treeSet);
                        switch (arrayList.size()%2) {
                            case 0:
                                if (arrayList.size()==0)
                                    break;
                                CrossroadMapped crossroadMapped1 = arrayList.get(arrayList.size() / 2 - 1);
                                CrossroadMapped crossroadMapped2 = arrayList.get(arrayList.size() / 2);
                                mediana = (double) (crossroadMapped1.getVehiclePassed() + crossroadMapped2.getVehiclePassed()) / 2;
                                break;
                            case 1:
                                mediana = (double) (arrayList.get(arrayList.size()/2).getVehiclePassed());
                                break;
                        }
                        for (CrossroadMapped crossroadMapped:arrayList){
                            if (crossroadMapped.getVehiclePassed()>mediana)
                                myCrossroadUpper.add(crossroadMapped);
                        }

                        collector.collect(new Tuple2<>(myCrossroadUpper,mediana));
                    }


                })
                .map(new MapFunction<Tuple2<ArrayList<CrossroadMapped>,Double>, String>() {
                    @Override
                    public String map(Tuple2<ArrayList<CrossroadMapped>, Double> crossroadMappedDoubleTuple2) throws Exception {

                        ListaSecondaQuery listaSecondaQuery=new ListaSecondaQuery();

                        ArrayList<CrossroadMapped> arrayList=new ArrayList<>();
                        crossroadMappedDoubleTuple2.f0.forEach(crossroadMapped -> arrayList.add(crossroadMapped));

                        listaSecondaQuery.setTimestamp(new Date());
                        listaSecondaQuery.setType(type);
                        listaSecondaQuery.setGloablMediana(crossroadMappedDoubleTuple2.f1);
                        listaSecondaQuery.setListOfCrossroad(arrayList);

                        Gson gson=new Gson();
                        System.out.println(gson.toJson(listaSecondaQuery));

                        return gson.toJson(listaSecondaQuery);

                    }
                })
                .addSink(producer);
    }
}
